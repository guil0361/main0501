using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitcher : MonoBehaviour
{
    public List<Camera> cameras; // Assign your cameras via the Unity Inspector
    private int currentCameraIndex = 0;

    void Start()
    {
        // Make sure only the first camera is active at the start
        for (int i = 0; i < cameras.Count; i++)
        {
            cameras[i].gameObject.SetActive(i == currentCameraIndex);
        }
    }

    void Update()
    {
        // Switch camera with 'Q' (previous) and 'E' (next)
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SwitchCamera(-1);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            SwitchCamera(1);
        }
    }

    void SwitchCamera(int direction)
    {
        // Deactivate current camera
        cameras[currentCameraIndex].gameObject.SetActive(false);

        // Calculate the next camera index
        currentCameraIndex = (currentCameraIndex + direction + cameras.Count) % cameras.Count;

        // Activate the next camera
        cameras[currentCameraIndex].gameObject.SetActive(true);
    }
}
