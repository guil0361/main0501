using UnityEngine;
using UnityEngine.UI;

public class CubeCounter : MonoBehaviour
{
    public Text cubeCounterText; // Drag your UI Text component here
    private int cubeCount = 0;

    // This function should be called whenever the crane collides with the cube
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cube")) // Make sure your cube has the tag "Cube"
        {
            IncreaseCubeCount();
        }
    }

    private void IncreaseCubeCount()
    {
        cubeCount++;
        UpdateCubeCounterText();
    }

    private void UpdateCubeCounterText()
    {
        cubeCounterText.text = "Cubes Lifted: " + cubeCount;
    }
}
