using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public Camera[] cameras;
    private int currentCameraIndex = 0;

    void Start()
    {
        if (cameras.Length == 0)
        {
            Debug.LogError("No cameras assigned to CameraSwitcher!");
            return;
        }

        // Enable the first camera and disable the others
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].gameObject.SetActive(i == currentCameraIndex);
        }
    }

    void Update()
    {
        if (cameras.Length == 0)
            return;

        // Switch cameras when 'C' is pressed
        if (Input.GetKeyDown(KeyCode.C))
        {
            // Disable current camera
            cameras[currentCameraIndex].gameObject.SetActive(false);

            // Move to the next camera
            currentCameraIndex = (currentCameraIndex + 1) % cameras.Length;

            // Enable the new camera
            cameras[currentCameraIndex].gameObject.SetActive(true);
        }
    }
}