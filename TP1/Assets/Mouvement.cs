using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    public float moveSpeed = 0.01f; // Normal movement speed
    public float sprintSpeed = 0.02f; // Sprint movement speed
    public float jumpForce = 5f; // Force applied to the jump
    private bool isGrounded = true; // Check if the player is on the ground
    private Rigidbody rb; // To access the Rigidbody component

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); // Get the Rigidbody component
    }

    // Update is called once per frame
    void Update()
    {
        // Set the movement speed to normal by default
        float speed = moveSpeed;

        // If LeftShift is held down, increase the speed for sprinting
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = sprintSpeed;
        }

        // Basic movement controls
        if (Input.GetKey(KeyCode.DownArrow)) 
        { 
            transform.Translate(Vector3.forward * speed); 
        } 
        if (Input.GetKey(KeyCode.UpArrow)) 
        { 
            transform.Translate(Vector3.back * speed); 
        }

        if (Input.GetKey(KeyCode.LeftArrow)) 
        { 
            transform.Rotate(Vector3.up, -2); 
        }

        if (Input.GetKey(KeyCode.RightArrow)) 
        { 
            transform.Rotate(Vector3.up, 2);
        }

        // Jumping logic (only jump if on the ground and the space key is pressed)
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false; // Set isGrounded to false while in the air
        }
    }

    // This function will check if the player has landed back on the ground
    private void OnCollisionEnter(Collision collision)
    {
        // If the player collides with something tagged "Ground", they are grounded
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true; // Reset isGrounded when back on the ground
        }
    }
}
